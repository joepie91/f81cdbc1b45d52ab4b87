var childProcess = require("child_process");

var electronPath = "./node_modules/electron-prebuilt/dist/electron";
var electronParameters = ["."];
var electronProcess = null;
var electronStopping = false;

startElectron = function(){
	electronProcess = childProcess.spawn(electronPath, electronParameters);
	
	electronProcess.stdout.pipe(process.stdout)
	
	electronProcess.on("close", function(code, signal){
		if (electronStopping) {
			console.log("Restarting electron as requested...");
		} else {
			console.log("Electron exited with code " + code + ", restarting...");
		}
		
		startElectron();
	});
}

restartElectron = function(){
	if (electronProcess != null) {
		electronStopping = true;
		electronProcess.kill();
	}
}
